import * as types from "../mutations-types";
import characterAPI from "@/api/character";

export const state = {
  character: [],
  loadingCharacter: false,
  characterDetail:null
};
export const actions = {
  getCharacter({ commit }, payload) {
    commit(types.REPLACE_LOADING_CHARACTER, { status: true });
    return new Promise((resolve, reject) => {
      characterAPI
        .get(payload)
        .then(response => {
          const character = response.data.results;
          commit(types.REPLACE_LOADING_CHARACTER, { status: false });
          commit(types.REPLACE_CHARACTER, { character });

          resolve(response);
        })
        .catch(error => {
          commit(types.REPLACE_LOADING_CHARACTER, { status: false });
          reject(error);
        });
    });
  },
  getById({ commit }, payload) {
    return new Promise((resolve, reject) => {
      characterAPI
        .getById(payload)
        .then(response => {
          const detail = response.data;
          commit(types.REPLACE_CURRENT_CHARACTER, { detail });

          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },

}
export const mutations = {
  [types.REPLACE_LOADING_CHARACTER](state, { status }) {
    state.loadingCharacter = status;
  },
  [types.REPLACE_CHARACTER](state, { character }) {
    state.character = character;
  },
  [types.REPLACE_CURRENT_CHARACTER](state, { detail }) {
    state.characterDetail = detail;
  },

};